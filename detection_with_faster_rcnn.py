import pandas as pd
import os, json

import gluoncv
from gluoncv import model_zoo, data, utils
import numpy as np
import mxnet as mx

net = model_zoo.get_model('faster_rcnn_resnet101_v1d_coco', pretrained=True, ctx=mx.gpu())

path = "./deploy/test/"
predict_csv = pd.read_csv("predict_partial.csv", sep=',')
image_names = list(predict_csv['guid/image'])
image_tail = "_image.jpg"
for image_name in image_names[1327:]:
    image_path = path + image_name + image_tail
    print(image_path)
    x, orig_img = data.transforms.presets.rcnn.load_test(image_path, short=1052, max_size=2000)
    box_ids, scores, bboxes = net(x.as_in_context(mx.gpu()))
    results = []
    for i in range(box_ids.shape[1]):
        if scores[0][i][0] > 0.5:
            result = [net.classes[int(box_ids[0][i].asnumpy().tolist()[0])], scores[0][i].asnumpy().tolist()[0], bboxes[0][i].asnumpy().tolist()]
            results.append(result)
    with open(path + image_name + "_image.faster.json", 'w') as f:
        f.write(json.dumps(results))