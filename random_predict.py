import os, sys, re

path = "D:/vscode-workspace/eecs598_selfdriving_car/deploy/test"
dirs = os.listdir(path)

# dirs.remove(".DS_Store")
print(dirs)

import pandas as pd
import random   

all_images = []
for dir in dirs:
	test_path = "{}/{}".format(path, dir)
	images = []
	for file in os.listdir(test_path):
		if re.match(r'\S+\.jpg', file):
			images.append("{}/{}".format(dir, file[:4]))
	images.sort()
	all_images.extend(images)

label = []
for i in range(len(all_images)):
    label.append(random.randint(0, 2))

dataframe = pd.DataFrame({'guid/image': all_images, 'label': label})
dataframe.to_csv("random_predict.csv", index=False, sep=',')