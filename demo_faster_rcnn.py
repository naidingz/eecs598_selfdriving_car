"""2. Predict with pre-trained Faster RCNN models
==============================================

This article shows how to play with pre-trained Faster RCNN model.

First let's import some necessary libraries:
"""

from matplotlib import pyplot as plt
import gluoncv
from gluoncv import model_zoo, data, utils
import numpy as np
import time
######################################################################
# Load a pretrained model
# -------------------------
#
# Let's get an Faster RCNN model trained on Pascal VOC
# dataset with ResNet-50 backbone. By specifying
# ``pretrained=True``, it will automatically download the model from the model
# zoo if necessary. For more pretrained models, please refer to
# :doc:`../../model_zoo/index`.
#
# The returned model is a HybridBlock :py:class:`gluoncv.model_zoo.FasterRCNN`
# with a default context of `cpu(0)`.

import mxnet as mx

print("begin load model")

net = model_zoo.get_model('faster_rcnn_resnet50_v1b_voc', pretrained=True, ctx=mx.gpu())

print("end load model")

######################################################################
# Pre-process an image
# --------------------
#
# Next we download an image, and pre-process with preset data transforms.
# The default behavior is to resize the short edge of the image to 600px.
# But you can feed an arbitrarily sized image.
#
# You can provide a list of image file names, such as ``[im_fname1, im_fname2,
# ...]`` to :py:func:`gluoncv.data.transforms.presets.rcnn.load_test` if you
# want to load multiple image together.
#
# This function returns two results. The first is a NDArray with shape
# `(batch_size, RGB_channels, height, width)`. It can be fed into the
# model directly. The second one contains the images in numpy format to
# easy to be plotted. Since we only loaded a single image, the first dimension
# of `x` is 1.
#
# Please beware that `orig_img` is resized to short edge 600px.

# im_fname = utils.download('https://github.com/dmlc/web-data/blob/master/' +
#                           'gluoncv/detection/biking.jpg?raw=true',
#                           path='38_box.jpg')
# im_fname = ('38_box.jpg', '48_box.jpg')
# x, orig_img = data.transforms.presets.rcnn.load_test('38_box.jpg')

######################################################################
# Inference and display
# ---------------------
#
# The Faster RCNN model returns predicted class IDs, confidence scores,
# bounding boxes coordinates. Their shape are (batch_size, num_bboxes, 1),
# (batch_size, num_bboxes, 1) and (batch_size, num_bboxes, 4), respectively.
#
# We can use :py:func:`gluoncv.utils.viz.plot_bbox` to visualize the
# results. We slice the results for the first image and feed them into `plot_bbox`:

# print(numpy.asarray(x).shape)
# print(len(x))

print("begin load test")

# x = mx.nd.load('my_list')

# fn_names = ['38_box.jpg', '92_box.jpg']
x, orig_img = data.transforms.presets.rcnn.load_test('./deploy/test/0410e7f5-4bfd-4d35-b623-9f8d6b87c179/0000_image.jpg', short=1052, max_size=2000)
# mx.nd.save('my_list', x)
print("end load test")

# net = model_zoo.get_model('faster_rcnn_resnet50_v1b_voc', pretrained=True, ctx=mx.gpu())

# box_ids, scores, bboxes = net(x.as_in_context(mx.gpu()))

print("begin predict")

box_ids, scores, bboxes = net(x.as_in_context(mx.gpu()))

# print(mx.nd.concat(box_ids[0], scores[0], bboxes[0], dim=1).shape)

# print(net(x[1]).argmax(1))

print("end predict")

# box_ids = box_ids.asnumpy()
# scores = scores.asnumpy()
# bboxes = bboxes.asnumpy()

# print(box_ids.shape, scores.shape, bboxes.shape)
# print(net.classes[6])


# result = np.concatenate((box_ids[0], scores[0], bboxes[0]), axis=1)
# # result = sorted(result, reverse=True, key=lambda a_entry: a_entry[1]) 
# result = result[result[:, 1].argsort()[::-1]]
# print(result.shape)

# print(type(result[:3]))

for i in range(box_ids.shape[1]):
    if scores[0][i][0] > 0.5:
        print(box_ids[0][i], scores[0][i], bboxes[0][i])

ax = utils.viz.plot_bbox(orig_img, bboxes[0], scores[0], box_ids[0], class_names=net.classes)

print(net.classes)

plt.show()

