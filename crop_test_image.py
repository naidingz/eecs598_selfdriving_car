from glob import glob
import pandas as pd
from PIL import Image
import os

width = 1914
height = 1052

box_width = 224
box_height = 224

boxes = []

for i in range(17):
    for j in range(9):
        if i == 16 and j == 8:
            box = [i * box_width / 2, j * box_height / 2, 122, 156]
        elif i == 16:
            box = [i * box_width / 2, j * box_height / 2, 122, box_height]
        elif j == 8:
            box = [i * box_width / 2, j * box_height / 2, box_width, 156]
        else:
            box = [i * box_width / 2, j * box_height / 2, box_width, box_height]

        boxes.append(box)


def crop(image_path, coords, saved_location):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    """
    image_obj = Image.open(image_path)
    cropped_image = image_obj.crop(coords)
    if not os.path.exists(os.path.dirname(saved_location)):
        os.makedirs(os.path.dirname(saved_location))
    cropped_image.save(saved_location)


predict_csv = pd.read_csv("random_predict.csv", sep=',')
image_names = list(predict_csv['guid/image'])

output_path = "./deploy/test-crop/"
input_path = "D:/vscode-workspace/eecs598_selfdriving_car/deploy/test/"
image_tail = "_image.jpg"

for image_name in image_names:
    image_path = input_path + image_name + image_tail
    for idx, box in enumerate(boxes):
        crop(image_path, (box[0], box[1], box[0] + box[2], box[1] + box[3]), output_path + image_name + "/" + str(idx) + "_box.jpg")