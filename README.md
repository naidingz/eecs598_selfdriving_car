## Task 1

### Detection

We detect vehicles with YOLOv3 pretrained on COCO dataset. To run detection for training and test set, you need to download darknet and pretrained weights first.

```bash
# download and compile darknet
git clone https://github.com/pjreddie/darknet
cd darknet
make

# download weights
wget https://pjreddie.com/media/files/yolov3.weights
```

Run a demo

```bash
./darknet detect cfg/yolov3.cfg yolov3.weights data/dog.jpg
```

Run detection on training and test set. Python 2 is necessary here. Make sure that `darknet_deploy.py` is under the directory `./darknet/python/`. You may need to change the path of `deploy` folder in the file.

```python
python ./python/darknet_deploy.py
```

 The code outputs a json file storing detection bounding box info for each snapshot.



### Classification

Crop bounding box from original image for training set according to json output

```bash
python3 crop_image.py
```

Organize cropped image into three folders (0, 1, 2) according to their categories

```bash
python3 organize_cropped_image.py
```

Train a classifier on the training set. Make sure you have installed latest version of `tensorflow` and `keras`.

```bash
python3 train_task1.py
```

Predict a result for task1

```bash
python3 predict_task1.py
```



## Task 2

Prepare the training set

```bash
python3 prepare_centroid.py
```

Train a classifier on the training set

```bash
python3 train_task2.py
```

Predict a result for task2

```bash
python3 predict_task2.py
```