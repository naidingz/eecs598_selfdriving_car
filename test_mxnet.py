import mxnet as mx 
def gpu_device(gpu_number=0):
    try:
        _ = mx.nd.array([1, 2, 3], ctx=mx.gpu(gpu_number))
    except mx.MXNetError as e:
        return str(e)
    return mx.gpu(gpu_number)

print(gpu_device())