import os, sys, re, keras
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json
from PIL import Image

def prepare_image(file):
    img_path = ''
    img = image.load_img(img_path + file, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return keras.applications.mobilenet.preprocess_input(img_array_expanded_dims)

predict_csv = pd.read_csv("random_predict.csv", sep=',')
image_names = list(predict_csv['guid/image'])

path = "./deploy/test/"
image_tail = "_image.jpg"

output_name = []
output_value = []

model = load_model('my_model_task2.h5')

for idx, image_name in enumerate(image_names):
    print(image_name)
    preprocessed_image = prepare_image(path + image_name + image_tail)
    predictions = model.predict(preprocessed_image)
    output_name.append(image_name + "/x")
    output_name.append(image_name + "/y")
    output_name.append(image_name + "/z")
    output_value.append(predictions[0][0])
    output_value.append(predictions[0][1])
    output_value.append(predictions[0][2])

dataframe = pd.DataFrame({'guid/image/axis': output_name, 'value': output_value})
dataframe.to_csv("predict_task2.csv", index=False, sep=',')