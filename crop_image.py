import os
import json
import pandas as pd

from PIL import Image
 
def crop(image_path, coords, saved_location):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    """
    image_obj = Image.open(image_path)
    cropped_image = image_obj.crop(coords)
    if not os.path.exists(os.path.dirname(saved_location)):
        os.makedirs(os.path.dirname(saved_location))
    cropped_image.save(saved_location)
    # cropped_image.show()
 
if __name__ == '__main__':
    labels = pd.read_csv("labels.csv", sep=',')
    image_names = list(labels['guid/image'])
    image_labels = list(labels['label'])

    path = './deploy/trainval/'
    crop_path = './deploy/trainval-crop/'
    image_tail = '_image.jpg'
    json_tail = '_image.jpg.json'

    crop_labels = []
    crop_names = []
    for image_idx, image_name in enumerate(image_names):
        with open(path + image_name + json_tail, 'r') as f:
            yolo_predict = json.loads(f.read())
            for idx, obj in enumerate(yolo_predict):
                if obj[0] == 'bicycle' or obj[0] == 'motocycle' or obj[0] == 'bus' or obj[0] == 'truck' or \
                    obj[0] == 'car' or obj[0] == 'airplane' or obj[0] == 'train' or obj[0] == 'boat':
                    crop(path + image_name + image_tail, (obj[2][0] - obj[2][2], obj[2][1] - obj[2][3], obj[2][0] + obj[2][2], obj[2][1] + obj[2][3]),
                        crop_path + image_name + "_crop_" + str(idx) + ".jpg")
                    crop_names.append(image_name + "_crop_" + str(idx))
                    crop_labels.append(image_labels[image_idx])
    
    dataframe = pd.DataFrame({'guid/crop': crop_names, 'label': crop_labels})
    dataframe.to_csv("crop_labels.csv", index=False, sep=',')