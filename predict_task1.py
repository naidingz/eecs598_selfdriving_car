import os, sys, re
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json
from PIL import Image

model = load_model('my_model_task1.h5')

def load_image(img_path, show=False):

    img = image.load_img(img_path, target_size=(224, 224))
    img_tensor = image.img_to_array(img) 
    img_tensor = np.expand_dims(img_tensor, axis=0)         
    img_tensor /= 255.                                      

    if show:
        plt.imshow(img_tensor[0])                           
        plt.axis('off')
        plt.show()

    return img_tensor

def crop(image_path, coords, saved_location):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    """
    image_obj = Image.open(image_path)
    cropped_image = image_obj.crop(coords)
    cropped_image.save(saved_location)

predict_csv = pd.read_csv("random_predict.csv", sep=',')
image_names = list(predict_csv['guid/image'])
image_labels = list(predict_csv['label'])

path = "./deploy/test/"
image_tail = "_image.jpg"
json_tail = "_image.jpg.json"

for image_idx, image_name in enumerate(image_names):
    print(path + image_name + image_tail)
    with open(path + image_name + json_tail, 'r') as f:
        yolo_predict = json.loads(f.read())
        if len(yolo_predict) == 0:
            image_labels[image_idx] = 0
        else:
            label = 2
            for idx, obj in enumerate(yolo_predict):
                if obj[0] == 'bicycle' or obj[0] == 'motocycle' or obj[0] == 'bus' or obj[0] == 'truck' or \
                    obj[0] == 'car' or obj[0] == 'airplane' or obj[0] == 'train' or obj[0] == 'boat':
                    crop(path + image_name + image_tail, (obj[2][0] - obj[2][2], obj[2][1] - obj[2][3], obj[2][0] + obj[2][2], obj[2][1] + obj[2][3]),
                        path + image_name + "_crop" + ".jpg")
                    crop_image = load_image(path + image_name + "_crop" + ".jpg")
                    pred = model.predict(crop_image)
                    label = np.argmax(pred)
                    break
            image_labels[image_idx] = label

dataframe = pd.DataFrame({'guid/image': image_names, 'label': image_labels})
dataframe.to_csv("predict_task1.csv", index=False, sep=',')