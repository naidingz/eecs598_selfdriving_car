import os
import pandas as pd
from shutil import copy

labels = pd.read_csv("crop_labels.csv", sep=',')
crop_names = list(labels['guid/crop'])
crop_labels = list(labels['label'])

print(len(crop_names))

crop_path = './deploy/trainval-crop/'
dst_path = './deploy/trainval-crop-category/'
image_tail = '.jpg'

for crop_idx, crop_name in enumerate(crop_names):
    # print(crop_idx, crop_name.replace("/", "_"), crop_labels[crop_idx])
    src = crop_path + crop_name + image_tail
    dst = dst_path + str(crop_labels[crop_idx]) + "/" + crop_name.replace("/", "_") + image_tail
    copy(src, dst)