import pandas as pd
import numpy as np
import keras

from IPython.display import Image
from keras.preprocessing import image
from keras.applications import MobileNet

centroids_csv = pd.read_csv("./centroids.csv", sep=',')
image_axis_names = list(centroids_csv['guid/image/axis'])
image_axis_values = list(centroids_csv['value'])

x_names = [None] * 7573
y = np.empty(shape=[7573, 3])

for i in range(0, len(image_axis_values), 3):
    y[int(i / 3)][0] = image_axis_values[i]
    y[int(i / 3)][1] = image_axis_values[i + 1]
    y[int(i / 3)][2] = image_axis_values[i + 2]
    x_names[int(i / 3)] = image_axis_names[i][:-2]

path = "./deploy/trainval/"
image_tail = "_image.jpg"

def prepare_image(file):
    img_path = ''
    img = image.load_img(img_path + file, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return keras.applications.mobilenet.preprocess_input(img_array_expanded_dims)

x = np.empty(shape=[7573, 224, 224, 3])
for idx, image_name in enumerate(x_names):
    x[idx] = prepare_image(path + image_name + image_tail)

np.save("x.npy", x)
np.save("y.npy", y)

